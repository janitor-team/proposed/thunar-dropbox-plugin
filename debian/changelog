thunar-dropbox-plugin (0.3.1-1) unstable; urgency=medium

  * d/copyright, d/repack-waf, d/watch:
    - Drop waf repack and move to new upstream.
  * New upstream version 0.3.1
    - Drop patch applied upstream.
  * d/copyright: Update attribution.
  * d/compat, d/control:
    - Drop d/compat in favor of debhelper-compat, bump to 12.
  * d/control:
    - Replace python2 B-D with cmake. (Closes: #938664)
    - Set R³ to no.
  * d/rules: Drop all waf overrides, set CMAKE_LIBRARY_PATH.
  * d/p/multiarch-libdir.patch: Use the multiarch directory for plugins.
  * Update Standards-Version to 4.4.1.

 -- Unit 193 <unit193@ubuntu.com>  Fri, 04 Oct 2019 01:00:09 -0400

thunar-dropbox-plugin (0.2.1+dfsg1-5) unstable; urgency=medium

  * d/p/01-port-to-thunarx-3.patch:
    - Use an upstream pull request to port to thunarx-3.
  * d/compat, d/control: Bump dh compat to 11.
  * d/control:
    - Adjust build-dep libthunarx-2-dev → libthunarx-3-dev.
    - Update Vcs-* fields, pointing to Salsa.
  * d/copyright:
    - Use https where possible.
    - Point source field to Github, update years.
    - Note why package is contrib and repacked.
  * Update Standards-Version to 4.1.5.

 -- Unit 193 <unit193@ubuntu.com>  Wed, 04 Jul 2018 08:32:20 -0400

thunar-dropbox-plugin (0.2.1+dfsg1-4) unstable; urgency=medium

  * Update Standards-Version to 4.0.0.
  * d/repack-waf, d/rules, d/watch:
    - Switch from get-orig-source target to repack script for waf.
  * d/control, d/copyright, d/watch: Update homepage.
  * d/control: Remove Micah Gersten from uploaders, no longer active in Debian.
  * d/copyright: Update years.

 -- Unit 193 <unit193@ubuntu.com>  Thu, 27 Jul 2017 21:44:14 -0400

thunar-dropbox-plugin (0.2.1+dfsg1-3) unstable; urgency=medium

  * Add Micah Gersten to uploaders.
  * Update standards-version to 3.9.8
  * d/control:
    - Add build-dep on python-minimal to provide 'python' (Closes: 832409)
    - Update vcs-* fields, use https.

 -- Unit 193 <unit193@ubuntu.com>  Fri, 05 Aug 2016 01:33:15 -0400

thunar-dropbox-plugin (0.2.1+dfsg1-2) unstable; urgency=medium

  * Re-upload to unstable.

 -- Unit 193 <unit193@ubuntu.com>  Mon, 27 Apr 2015 21:01:53 -0400

thunar-dropbox-plugin (0.2.1+dfsg1-1) experimental; urgency=medium

  * Imported Upstream version 0.2.1+dfsg1.
  * debian/install, debian/control, debian/rules:
    - Remove workaround to install the plugin into the multiarch
      location, upstream added --libdir support.
      + Drop dh_install override.
      + Change --destdir to install to debian/thunar-dropbox-plugin/
      + Add --libdir=/usr/lib/$(DEB_HOST_MULTIARCH)/ to configure.
      + Remove pkg-config build-dep and call.
  * d/copyright: Update years.

 -- Unit 193 <unit193@ubuntu.com>  Sat, 14 Mar 2015 21:29:14 -0400

thunar-dropbox-plugin (0.2.0+dfsg1-2) unstable; urgency=medium

  * d/rules: Enable verbose build and disable cache.
  * Move to contrib.
  * Update Standards-Version to 3.9.6.

 -- Unit 193 <unit193@ubuntu.com>  Thu, 09 Oct 2014 20:42:03 -0400

thunar-dropbox-plugin (0.2.0+dfsg1-1) unstable; urgency=medium

  * Initial release. (LP: #1000416)

 -- Unit 193 <unit193@ubuntu.com>  Tue, 05 Aug 2014 20:31:51 -0400
